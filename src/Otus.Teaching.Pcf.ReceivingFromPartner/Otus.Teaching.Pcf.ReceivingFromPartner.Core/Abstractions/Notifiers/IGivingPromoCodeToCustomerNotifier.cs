﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Notifiers
{
    public interface IGivingPromoCodeToCustomerNotifier
    {
        Task GivePromoCodeToCustomer(PromoCode promoCode);
    }
}
