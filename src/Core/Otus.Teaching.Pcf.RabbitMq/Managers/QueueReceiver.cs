﻿using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.RabbitMq.Options;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.RabbitMq.Managers
{
    public interface IQueueReceiver
    {
        void Receive(Func<string, Task> handleMessageAsync);
        void Receive(Action<string> handleMessage);
    }

    internal class QueueReceiver : IQueueReceiver
    {
        private readonly IRabbitManager _rabbitManager;
        private readonly QueueSetting _queueSetting;

        public QueueReceiver(IRabbitManager rabbitManager, IOptions<QueueSetting> options)
        {
            _rabbitManager = rabbitManager;
            _queueSetting = options.Value;
        }

        public void Receive(Func<string, Task> handleMessageAsync)
        {
            _rabbitManager.Consume(_queueSetting, async (message, _) => await handleMessageAsync!.Invoke(message));
        }

        public void Receive(Action<string> handleMessage)
        {
            _rabbitManager.Consume(_queueSetting, (message, _) => handleMessage!.Invoke(message));
        }
    }
}
