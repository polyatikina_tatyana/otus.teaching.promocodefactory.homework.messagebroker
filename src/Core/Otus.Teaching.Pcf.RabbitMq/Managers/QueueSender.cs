﻿using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.RabbitMq.Options;

namespace Otus.Teaching.Pcf.RabbitMq.Managers
{
    public interface IQueueSender
    {
        void Send(string message, string routingKey, string correlationId = null);

        void Send<T>(T message, string routingKey, string correlationId = null)
            where T : class;
    }

    internal class QueueSender : IQueueSender
    {
        private readonly IRabbitManager _rabbitManager;
        private readonly QueueSetting _queueSetting;

        public QueueSender(IRabbitManager rabbitManager, IOptions<QueueSetting> options)
        {
            _rabbitManager = rabbitManager;
            _queueSetting = options.Value;
        }

        public void Send(string message, string routingKey, string correlationId = null)
        {
            _rabbitManager.Publish(message, _queueSetting, routingKey, correlationId);
        }

        public void Send<T>(T message, string routingKey, string correlationId = null)
            where T : class
        {
            _rabbitManager.Publish(message, _queueSetting, routingKey, correlationId);
        }
    }
}
