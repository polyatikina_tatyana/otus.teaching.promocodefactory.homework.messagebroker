﻿using System;

namespace Otus.Teaching.Pcf.RabbitMq.Options
{
    public class QueueListenerSetting
    {
        public bool Enabled { get; set; } = true;
        public int Period { get; set; } = (int)TimeSpan.FromSeconds(15).TotalMilliseconds;
    }
}
